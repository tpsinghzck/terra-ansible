# terra-ansible

Ansible project to run terraform. It consists of 3 roles:
- terra-init: Used to dynamically init backend
- terra-plan: Used to create a terraform plan based on tfvars file
- terra-exec: Used to apply the state specified in the .tfplan file

Requirements: 
1. Ansible
2. Boto3
3. AWSCLI
4. terraform
5. An AWS Account
6. An ansible_vars.yml containing variables with information regrading bucket, region, etc. Please check sample_ansible_vars.yml for refrence


There are three main files:
- main.yaml: Run this if you want to execute all roles and create terraform from scratch
- plan.yaml: Execute this if you want to only create a plan file. The file will be stored on local as well as S3
- exec.yaml: Use this if you'd only like to do terraform apply from an existing plan file.

To run the playbook use the command

`ansible-playbook <Your playbook>.yaml`